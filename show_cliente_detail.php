<?php
require "conexion.php";
$cli = new Cliente();
$clienteConcreto = $cli->getClienteConcreto(["CLIENTE_COD"]);

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tabla cliente <?php foreach ($clienteConcreto as $cliente) echo $cliente["CLIENTE_COD"] ?></title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </head>
  <body>
    <table align=center border=1px>
          <td style="text-align:center;padding:25px">Codigo de cliente</td>
          <td style="text-align:center;padding:25px">Nombre</td>
          <td style="text-align:center;padding:25px">Telefono</td>
          <td style="text-align:center;padding:25px">Direccion</td>
          <td style="text-align:center;padding:25px">Ciudad</td>
          <td style="text-align:center;padding:25px">Estado</td>
          <td style="text-align:center;padding:25px">Area</td>
          <td style="text-align:center;padding:25px">Codigo de repartidor</td>
          <td style="text-align:center;padding:25px">Limite credito</td>
          <td style="text-align:center;padding:25px">Observaciones</td>
          <td style="text-align:center;padding:25px">Eliminar fila</td>
      </tr>
      <?php foreach ($clienteConcreto as $cliente) {
        echo "<tr>";
        echo "<td style='text-align:center'><a href='show_cliente_detail.php?id=".$cliente["CLIENTE_COD"]."'>".$cliente["CLIENTE_COD"]."</a></td>";
        echo "<td style='text-align:center'>".$cliente["NOMBRE"]."</td>";
        echo "<td style='text-align:center'>".$cliente["TELEFONO"]."</td>";
        echo "<td style='text-align:center'>".$cliente["DIREC"]."</td>";
        echo "<td style='text-align:center'>".$cliente["CIUDAD"]."</td>";
        echo "<td style='text-align:center'>".$cliente["ESTADO"]."</td>";
        echo "<td style='text-align:center'>".$cliente["AREA"]."</td>";
        echo "<td style='text-align:center'>".$cliente["REPR_COD"]."</td>";
        echo "<td style='text-align:center'>".$cliente["LIMITE_CREDITO"]."</td>";
        echo "<td style='text-align:center'>".$cliente["OBSERVACIONES"]."</td>";
        echo "<td style='text-align:center'><a href='delete.php?id=".$cliente["CLIENTE_COD"]."'>Eliminar</a></td>";
      }
      ?>
  </body>
</html>
