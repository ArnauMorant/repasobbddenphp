<?php
require "conexion.php";
$cli = new Cliente();
$lisCli = $cli->getListaClientes();

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tabla cliente</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </head>
  <body>
    <table align=center border=1px>
          <td style="text-align:center;padding:25px">Codigo de Cliente</td>
          <td style="text-align:center;padding:25px">Nombre</td>
          <td style="text-align:center;padding:25px">Telefono</td>
      </tr>
      <?php foreach ($lisCli as $cliente) {
        echo "<tr>";
        echo "<td style='text-align:center'><a href='show_cliente_detail.php?id=".$cliente["CLIENTE_COD"]."'>".$cliente["CLIENTE_COD"]."</a></td>";
        echo "<td style='text-align:center'>".$cliente["NOMBRE"]."</td>";
        echo "<td style='text-align:center'>".$cliente["TELEFONO"]."</td>";
      }
      ?>
  </body>
</html>
