<?php
require "conexion.php";
$cli = new Cliente();
$cli2 = new borrarCliente();
$lisCli = $cli->getClienteConcreto(["CLIENTE_COD"]);
$delCli = $cli2->deleteClient(["id"]);

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Eliminar cliente</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </head>
  <body>      
      <?php foreach ($lisCli as $cliente) {
        echo "Has eliminado el cliente con el código: ".$cliente["CLIENTE_COD"]."<br>";
      }
      echo "<a href='show_cliente.php'>Volver atrás</a>";
      ?>
  </body>
</html>