<?php
class Conexion{
    public $conexion=null;
    private $host = "localhost";
    private $username = "root";
    private $pass = "";
    private $db = "empresa";

    protected function conectar(){
        $this->conexion = new mysqli($this->host,$this->username,$this->pass,$this->db);
        if ($this->conexion->connect_errno) {
            echo "¡¿PERO QUE ES ESTO?! Esta cosa no puede conectarse a MySQL :  (".$this->conexion->connect_errno.")".$this->conexion->connect_error;
        }
    }
}

class Cliente extends Conexion{
    function __construct(){
        $this->conectar();
    }
    
    function getListaClientes(){
        $listaClientes = $this->conexion->query("SELECT * FROM cliente");
        return $listaClientes;
    }

    function getClienteConcreto($clienteConcreto){
        $clienteConcreto = $this->conexion->query("SELECT * FROM cliente WHERE CLIENTE_COD =".$_GET['id']);
        return $clienteConcreto;
    }
}

class borrarCliente extends Conexion{
    function __construct(){
        $this->conectar();
    }
    function deleteClient($deleteCliente){
        $deleteCliente = $this->conexion->query("DELETE FROM cliente WHERE CLIENTE_COD =".$_GET['id']);
        return $deleteCliente;
    }
}